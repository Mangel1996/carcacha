<?php

namespace negocio\controlador;

use negocio\generico\GenericoControlador;

class MenuControlador extends GenericoControlador {
    
    
    public function __construct(&$cnn) {
        parent::__construct($cnn);
        parent::validarSesion();
    }
    
    public function index(){
        $propietario = $_SESSION['propietario'];
        include_once CARPETA_PRINCIPAL. '/vista/menu.php';
    }
        public function indexCarcacha(){
        $propietario = $_SESSION['propietario'];
        include_once CARPETA_PRINCIPAL. '/vista/gestionarcarcacha.php';
    }
  
    
}
