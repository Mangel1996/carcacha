<?php

namespace negocio\controlador;

use negocio\generico\GenericoControlador;
use persistencia\dao\CarroDAO;
use persistencia\dao\FotoDAO;
use persistencia\vo\Carro;
use persistencia\vo\Foto;
use const CARPETA_PRINCIPAL;

class CarroControlador extends GenericoControlador {
private $carroDAO;

public function __construct(&$cnn) {
        parent::__construct($cnn);
        $this->carroDAO = new CarroDAO($cnn);
}
    public function guardarCarcacha() {
        $propietario = $_SESSION['propietario'];
        $id = $propietario->getIdPropietario();
        $carro = new Carro();
        $carro->convertir($_POST); //Mirar como quitar esta linea
        $carro->setPropietario($propietario);
        $listaArchivos = $_FILES['fot_nombre'];
        $foto = $_FILES['fot_nombre'];
        $listaImagenes = array();
        for ($i = 0; $i < 4; $i++) {
            if ($foto['size'][$i]==0) {
                continue;
            }
            // hacer validacion que sea imagen
            $ext = str_replace('image/', '', $foto['type'][$i]);
            $nombreFinal = $id . '_' . round(microtime(true) * 1000) . '_' . rand(0, 1000) . '.' . $ext;
            $ruta = CARPETA_PRINCIPAL . '/archivos/' . $nombreFinal;
            move_uploaded_file($foto['tmp_name'][$i], $ruta);
            $objFoto = new Foto();
            $objFoto->setNombre($foto['name'][$i]);
            $objFoto->setRuta($nombreFinal);
            $listaImagenes[] = $objFoto;
        }
        $idCarro = $this->carroDAO->insertar($carro);
        $carro->setIdCarro($idCarro);
        foreach ($listaImagenes as $imagen) {
            $imagen->setCarro($carro);
            $fotoDAO = new FotoDAO($this->cnn);
            $fotoDAO->insertar($imagen);
        }
            
    }

}
