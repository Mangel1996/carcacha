<?php

namespace negocio\controlador;

use Exception;
use negocio\excepcion\CarcachaExcepcion;
use negocio\generico\GenericoControlador;
use negocio\util\Validacion;
use persistencia\dao\PropietarioDAO;
use persistencia\vo\PropietarioVO;
use const CARPETA_PRINCIPAL;
use const RUTA_PRINCIPAL;

class PropietarioControlador extends GenericoControlador {

    /**
     * 
     *  @var PropietarioDAO
     */
    private $propietarioDAO;

    public function __construct(&$cnn) {
        parent::__construct($cnn);
        $this->propietarioDAO = new PropietarioDAO($cnn);
    }

    public function guardar() {
        try {
            $propietario = new PropietarioVO();
            $propietario->convertir($_POST);
            Validacion::obligatorio($propietario->getNombre(), 'El nombre es obligatorio');
            Validacion::obligatorio($propietario->getApellido(), 'El apellido es obligatorio');
            Validacion::obligatorio($propietario->getCorreo(), 'El correo es obligatorio');
            Validacion::obligatorio($propietario->getClave(), 'La contraseña es obligatoria');
            if ($propietario->getClave() != $_POST['pro_clave_confirmacion']) {
                throw new CarcachaExcepcion('Las contraseñas no coinciden');
            }
            $nombreFinal = '';
            if (isset($_FILES['pro_foto_temp'])) {
                $foto = $_FILES['pro_foto_temp'];
                $ext = str_replace('image/', '', $foto['type']);
                $nombreFinal = round(microtime(true) * 1000) . '_' . rand(0, 1000) . '.' . $ext;
                $ruta = CARPETA_PRINCIPAL . '/archivos/' . $nombreFinal;
                move_uploaded_file($foto['tmp_name'], $ruta);
            }
            $propietario->setFoto($nombreFinal);
            $this->propietarioDAO->insertar($propietario);
        } catch (CarcachaExcepcion $e) {
            print_r($e->getMessage());
        } catch (Exception $e) {
            print_r('Error al registrar');
        }
    }

    public function autenticar() {
        $correo = $_POST['pro_correo'];
        $clave = $_POST['pro_clave'];
        $autenticar = $this->propietarioDAO->autenticar($correo, $clave);
        if (is_null($autenticar)) {
            header('location: ' . RUTA_PRINCIPAL);
            return;
        }
        session_start();
        $_SESSION['propietario'] = $autenticar;
        header('location: ' . MENU['url']);
    }

    public function indexRegistrar() {
        include_once CARPETA_PRINCIPAL . '/vista/registrarusuario.php';
    }

}
