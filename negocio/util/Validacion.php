<?php

namespace negocio\util;

use negocio\excepcion\CarcachaExcepcion;

class Validacion {

    public static function obligatorio($valor, $mensaje) {
        if (empty($valor)) {
            throw new CarcachaExcepcion($mensaje);
        }
    }

}
