<?php

namespace persistencia\dao;

use persistencia\generico\GenericoDAO;

class GastoDAO extends GenericoDAO {

    public function __construct($cnn) {
        parent::__construct($cnn, 'gasto');
    }

}
