<?php

namespace persistencia\dao;

use persistencia\generico\GenericoDAO;

class FotoDAO extends GenericoDAO {

    public function __construct($cnn) {
        parent::__construct($cnn, 'foto');
    }

}
