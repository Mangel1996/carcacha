<?php

namespace persistencia\dao;

use persistencia\generico\GenericoDAO;
use persistencia\vo\PropietarioVO;

class PropietarioDAO extends GenericoDAO {

    public function __construct($cnn) {
        parent::__construct($cnn, 'propietario');
    }

    public function autenticar($correo, $clave) {
        $sql = 'select * from propietario where correo = :correo and clave = :clave';
        $sentencia = $this->cnn->prepare($sql);
        $sentencia->bindParam(':correo', $correo);
        $sentencia->bindParam(':clave', $clave);
        $sentencia->execute();
        $resultado = $sentencia->fetchAll();
        if (empty($resultado)) {
            return;
        }
        $registro = $resultado[0];
        $propietario = new PropietarioVO;
        $propietario->setIdPropietario($registro['id_propietario']);
        $propietario->setNombre($registro['nombre']);
        $propietario->setApellido($registro['apellido']);
        $propietario->setCorreo($registro['correo']);
        $propietario->setFechaNacimiento($registro['fecha_nacimiento']);
        $propietario->setTelefono($registro['telefono']);
        $propietario->setFoto($registro['foto']);
        $propietario->setCedula($registro['cedula']);
        return $propietario;
    }

}
