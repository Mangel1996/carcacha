<?php

namespace persistencia\dao;
use persistencia\generico\GenericoDAO;


class CarroDAO extends GenericoDAO {
    public function __construct($cnn) {
        parent::__construct($cnn, 'carro');
    }
}
