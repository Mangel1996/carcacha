<?php

namespace persistencia\vo;

use persistencia\generico\IGenericoVO;

class Gasto implements IGenericoVO {

    private $idGasto;
    private $nombre;
    private $descripcion;
    private $valor;
    private $fecha;

    /**
     *
     * @var Carro 
     */
    private $carro;

    function getIdGasto() {
        return $this->idGasto;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getValor() {
        return $this->valor;
    }

    function getFecha() {
        return $this->fecha;
    }

    function setIdGasto($idGasto) {
        $this->idGasto = $idGasto;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setValor($valor) {
        $this->valor = $valor;
    }

    function setFecha($fecha) {
        $this->fecha = $fecha;
    }
    
    function getCarro() {
        return $this->carro;
    }

    function setCarro(Carro $carro) {
        $this->carro = $carro;
    }

    
    public function convertir($info) {
        
    }

    public function getAtributos() {
        $info = array();
        $info['id_gasto'] = $this->idGasto;
        $info['nombre'] = $this->idGasto;
        $info['descripcion'] = $this->idGasto;
        $info['valor'] = $this->valor;
        $info['fecha'] = $this->fecha;
        $info['id_carro'] = is_null($this->carro) ? NULL : $this->carro->getIdCarro();
    }

}
