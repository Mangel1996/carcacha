<?php

namespace persistencia\vo;

use persistencia\generico\IGenericoVO;

class Foto implements IGenericoVO {

    private $idFoto;
    private $nombre;
    private $ruta;

    /**
     *
     * @var Carro 
     */
    private $carro;

    function getIdFoto() {
        return $this->idFoto;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getRuta() {
        return $this->ruta;
    }

    function getCarro() {
        return $this->carro;
    }

    function setIdFoto($idFoto) {
        $this->idFoto = $idFoto;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setRuta($ruta) {
        $this->ruta = $ruta;
    }

    function setCarro(Carro $carro) {
        $this->carro = $carro;
    }

    public function convertir($info) {
        
    }

    public function getAtributos() {
       $info = array();
       $info['id_foto']= $this->idFoto;
       $info['nombre']= $this->nombre;
       $info['ruta']= $this->ruta;
       $info['id_carro']= is_null($this->carro) ? NULL : $this->carro->getIdCarro();
       return $info;
    }

}
