<?php

namespace persistencia\vo;

use persistencia\generico\IGenericoVO;

class Carro implements IGenericoVO {

    private $idCarro;
    private $marca;
    private $referencia;
    private $modelo;
    private $descripcion;
    private $titulo;
    private $placa;
    private $color;
    private $numeroPasajeros;

    /**
     *
     * @var PropietarioVO
     */
    private $propietario;

    function getNumeroPasajeros() {
        return $this->numeroPasajeros;
    }

    function setNumeroPasajeros($numeroPasajeros) {
        $this->numeroPasajeros = $numeroPasajeros;
    }

    function getIdCarro() {
        return $this->idCarro;
    }

    function getMarca() {
        return $this->marca;
    }

    function getReferencia() {
        return $this->referencia;
    }

    function getModelo() {
        return $this->modelo;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getPlaca() {
        return $this->placa;
    }

    function getColor() {
        return $this->color;
    }


    function getPropietario() {
        return $this->propietario;
    }

    function setIdCarro($idCarro) {
        $this->idCarro = $idCarro;
    }

    function setMarca($marca) {
        $this->marca = $marca;
    }

    function setReferencia($referencia) {
        $this->referencia = $referencia;
    }

    function setModelo($modelo) {
        $this->modelo = $modelo;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setPlaca($placa) {
        $this->placa = $placa;
    }

    function setColor($color) {
        $this->color = $color;
    }


    function setPropietario(PropietarioVO $propietario) {
        $this->propietario = $propietario;
    }

    public function convertir($info) {
        $atributos = array_keys(get_object_vars($this));
        foreach ($atributos as $nombreAtributos) {
            if (isset($info['car_' . $nombreAtributos])) {
                $this->$nombreAtributos = $info['car_' . $nombreAtributos];
            }
        }
    }

    public function getAtributos() {
        $info = array();
        $info['id_carro'] = $this->idCarro;
        $info['marca'] = $this->marca;
        $info['referencia'] = $this->referencia;
        $info['modelo'] = $this->modelo;
        $info['descripcion'] = $this->descripcion;
        $info['titulo'] = $this->titulo;
        $info['placa'] = $this->placa;
        $info['color'] = $this->color;
        $info['numero_pasajeros'] = $this->numeroPasajeros;
        $info['id_propietario'] = is_null($this->propietario) ? NULL : $this->propietario->getIdPropietario();
        return $info;
    }

}
