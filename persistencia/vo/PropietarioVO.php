<?php

namespace persistencia\vo;

use persistencia\generico\IGenericoVO;

class PropietarioVO implements IGenericoVO {

    private $idPropietario;
    private $nombre;
    private $apellido;
    private $correo;
    private $foto;
    private $clave;
    private $cedula;
    private $telefono;
    private $fechaNacimiento;
    private $listaCarros = array();

    function getIdPropietario() {
        return $this->idPropietario;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getFoto() {
        return $this->foto;
    }

    function getClave() {
        return $this->clave;
    }

    function getCedula() {
        return $this->cedula;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    function setIdPropietario($idPropietario) {
        $this->idPropietario = $idPropietario;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function setFoto($foto) {
        $this->foto = $foto;
    }

    function setClave($clave) {
        $this->clave = $clave;
    }

    function setCedula($cedula) {
        $this->cedula = $cedula;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setFechaNacimiento($fechaNacimiento) {
        $this->fechaNacimiento = $fechaNacimiento;
    }

    public function getAtributos() {
        $atributos = array();
        $atributos['id_propietario'] = $this->idPropietario;
        $atributos['nombre'] = $this->nombre;
        $atributos['apellido'] = $this->apellido;
        $atributos['correo'] = $this->correo;
        $atributos['foto'] = $this->foto;
        $atributos['clave'] = $this->clave;
        $atributos['cedula'] = $this->cedula;
        $atributos['telefono'] = $this->telefono;
        $atributos['fecha_nacimiento'] = $this->fechaNacimiento;
        return $atributos;
    }

    public function convertir($info) {
        $atributos = array_keys(get_object_vars($this));
        unset($atributos['listaCarros']);
        foreach ($atributos as $nombreAtributos) {
            if (isset($info['pro_' . $nombreAtributos])) {
                $this->$nombreAtributos = $info['pro_' . $nombreAtributos];
            }
        }
    }

    function getListaCarros() {
        return $this->listaCarros;
    }

    function setListaCarros($listaCarros) {
        $this->listaCarros = $listaCarros;
    }

}
