<?php
$proyecto = '/carcacha/';
define("CARPETA_PRINCIPAL",__DIR__);
define("CARPETA_ARCHIVOS", $proyecto . 'archivos/');
define("CARPETA_RECURSOS",$proyecto .'vista/');
define("RUTA_PRINCIPAL",$rutaPrincipal);
define("USUARIO_AUTENTICAR", array('url' => $rutaPrincipal . '/usuario/autenticar', 'controlador' => 'PropietarioControlador','metodo'=>'autenticar'));
define("MENU", array('url' => $rutaPrincipal . '/menu', 'controlador' => 'MenuControlador','metodo'=>'index'));
define("USUARIO_REGISTRAR", array('url' => $rutaPrincipal . '/usuario/registrar', 'controlador' => 'PropietarioControlador','metodo'=>'indexRegistrar'));
define("USUARIO_GUARDAR", array('url' => $rutaPrincipal . '/usuario/guardar', 'controlador' => 'PropietarioControlador','metodo'=>'guardar'));
define("GESTIONAR_CARCACHA", array('url' => $rutaPrincipal . '/menu/gestionar/carcacha', 'controlador' => 'MenuControlador','metodo'=>'indexCarcacha'));
define("GESTIONAR_CARCACHA_GUARDAR", array('url' => $rutaPrincipal . '/menu/gestionar/carcacha/guardar', 'controlador' => 'CarroControlador','metodo'=>'guardarCarcacha'));

