<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registrar Usuario</title>
        <link href="<?= CARPETA_RECURSOS ?>css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="<?= CARPETA_RECURSOS ?>js/bootstrap.min.js" type="text/javascript"></script>
        <style>
            .header {
                color: #36A0FF;
                font-family: TIMES NEW ROMAN;
                font-size: 27px;
                padding: 10px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            
        
        <form class="form-horizontal" action="<?= USUARIO_GUARDAR['url'] ?>" method="post" enctype="multipart/form-data">
                <div>
                </div>
                <legend class="text-center header">Registro Usuario</legend>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>

                    <div class="col-md-4">
                        <input type="text" id="pro_nombre" name="pro_nombre" class="form-control" placeholder="Nombre">
                    </div>
                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>
                    <div class="col-md-4">
                        <input type="text" id="pro_apellido" name="pro_apellido" class="form-control" placeholder="Apellido">
                    </div>
                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>
                    <div class="col-md-3">
                        <input type="email" id="pro_correo" name="pro_correo" class="form-control" placeholder="Correo">
                    </div>

                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>
                    <div class="col-md-4">
                        <input type="file" id="pro_foto_temp" name="pro_foto_temp" class="form-control" placeholder="Foto">
                    </div>

                </div>
                <div class="form-group ">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>
                    <div class="col-md-3">
                        <input type="password" id="pro_clave" name="pro_clave" class="form-control" placeholder="Clave">
                        <span class=" text-center"></span><br>
                        <div >
                            <input type="password" id="pro_clave_confirmacion" name="pro_clave_confirmacion" class="form-control" placeholder="Clave Confirmación">

                    </div>
                </div>
                </div>
                    
                <div class="form-group ">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>
                    <div class="col-md-3">
                        <input type="number" id="pro_cedula" name="pro_cedula" class="form-control" placeholder="Cedula"> 
                    </div>
                </div>
                <div class="form-group ">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>
                    <div class="col-md-3">
                        <input type="number" id="pro_telefono" name="pro_telefono" class="form-control" placeholder="Telefono">
                    </div>

                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>
                    <div class="col-md-3">
                        <label>Fecha de nacimiento</label>
                        <input type="date" id="pro_fecha_nacimiento" name="pro_fecha_nacimiento" class="form-control" placeholder=" Fecha de Nacimiento">
                    </div>


                </div>
                <div class="form-group">
                    <span class="col-md-1 col-md-offset-3 text-center"></span>
                    <div class="col-md-4">
                        <button class="btn btn-success col-md-12">Registrar</button>
                    </div>

                </div>

        </form> 
            
            </div>
    </body>
</html>
