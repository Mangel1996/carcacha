<!DOCTYPE html>
<html>


    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="<?= CARPETA_RECURSOS ?>css/bootstrap.css"/>
        <script type="text/javascript" src="<?= CARPETA_RECURSOS ?>js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="<?= CARPETA_RECURSOS ?>js/bootstrap.min.js"></script>
        <style>

            #menu{
                color: #FFF;
                clear: both;

            }
            #menu a{
                color: white;
                display: block;
                margin-top: 3px;
                padding: 0.7em;
                font-family: verdana;
                font-size: 13px;
                text-decoration: none;
                background-color: #6A4;
            }
            #menu a:hover{
                background-color: #28a4c9;
                padding-top: 1em;
                padding-bottom: 0.5em;
                border: 1px solid #333;
                border-radius: 0.5em 0.5em 0em 0em;
                cursor: pointer;
            }
            ul.navbar {
                position: absolute;
                left: 1px;
                width: 14em
            }
            body{
                
            }
            #todo{
                margin: 1px;
            }
            
            #titulo{
                margin: 2px;
                background-color: #1b6d85;
                padding-bottom: 1em;
                border-bottom: 5px solid #ccc;
                min-height: 50px;
                
            }
            #contenido{
                padding-left: 20em;
            }
            #nombre{
                color: white; 
                font-family: TIMES NEW ROMAN;
                padding-right: 35px;
                padding-top: 10px;
            }
            #imagen{
                
                height: 80px;
                width: 80px;
                border-radius: 50%;
                
            }
            #espacio{
                padding-right:30px;
            }
            a{
                padding-right: 2em;
            }
        </style>
    </head>
    <body>
        <div id="todo">
        
            <div id="titulo" class="row" align="right">
                <div id="espacio">
                <img id="imagen" src="<?= CARPETA_ARCHIVOS . $propietario->getFoto() ?>"/>
                </div>
                <div id="nombre" ><strong><?= strtoupper($propietario->getNombre()) . '<br> ' . strtoupper($propietario->getApellido()) ?></strong></div>
                <a href="" style="color: blue;">Cerrar sesión</a>
            </div>


            <div id="menu">
                <ul id="menus" class="navbar">
                    <li><a href="<?= GESTIONAR_CARCACHA['url'] ?>">Gestionar Carcacha</a></li>
                    <li><a href="acercade.php">Acerca de</a></li>
                    <li><a href="contacto.php">Contactos</a></li>
                </ul>
            </div>

            <div id="contenido" >
                <h1>Bienvenidos</h1>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
            </div>
            </div>

        <script type="text/javascript">
            $('div#menu a').on('click', eventoClick);
            function cargarVista(url) {
                $.get(url, procesarRespuesta);
            }
            function procesarRespuesta(html) {
                $('#contenido').empty().append(html);
            }
            function eventoClick(e) {
                detener(e);
                var vinculo = $(this);
                var url = vinculo.attr('href');
                cargarVista(url);
            }
            function detener(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
                if (e.returnValue) {
                    e.returnValue = false;
                }
            }
        </script>
    </body>
</html>
