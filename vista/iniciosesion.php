<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="<?= CARPETA_RECURSOS ?>css/bootstrap.css"/>

        <link rel="stylesheet" type="text/css" href="<?= CARPETA_RECURSOS ?>css/estilos.css">
    </head>
</head>
<body>
    <div id="Contenedor">
        <div class="Icon">
            <!--Icono de usuario-->
            <img src="<?= CARPETA_RECURSOS ?>css/1.png" style="height:90px; width:90px; border-radius: 50px;">
        </div>
        <div class="ContentForm">
            <form action="<?= USUARIO_AUTENTICAR['url'] ?>" method="POST" name="FormEntrar">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input type="email" placeholder="Correo" name="pro_correo" class="form-control" placeholder="Correo" id="Correo" aria-describedby="sizing-addon1" required>
                </div>
                <br>
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="sizing-addon1"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" placeholder="Clave" name="pro_clave" class="form-control" placeholder="******" aria-describedby="sizing-addon1" required>
                </div>
                <br>
                <button class="btn btn-lg btn-primary btn-block btn-signin" id="IngresoLog" type="submit">Entrar</button>
                <div class="opcioncontra"><a href="">Olvidaste tu contraseña?</a><br><a href="<?= USUARIO_REGISTRAR['url'] ?>"> Registrar </a></div>
            </form>
        </div>	
    </div>
</body>
<script type="text/javascript" src="<?= CARPETA_RECURSOS ?>js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?= CARPETA_RECURSOS ?>js/bootstrap.min.js"></script>
</html>
